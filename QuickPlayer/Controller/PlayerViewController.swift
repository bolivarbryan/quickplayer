//
//  PlayerViewController.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController {
    //MARK: Properties
    @IBOutlet weak var currentCoverImage: UIImageView!
    @IBOutlet weak var audioNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var proggressView: UIProgressView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageHeightConstraint.constant = UIScreen.main.bounds.width - 16
        AudioManager.sharedInstance.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    @IBAction func userPressedPlayButton(_ sender: Any) {
        
        if !self.playButton.isSelected {
            //starting pause
            self.loadPlaylist()
        } else {
            self.stopPlayList()
        }
    }
    
    @IBAction func userPressedNextAudio(_ sender: Any) {
        //TODO: move back button action to a new one called ReplaySong
        AudioManager.sharedInstance.next()
    }
}

extension PlayerViewController {
    
    //MARK: Methods
    func loadPlaylist() {
        AudioManager.sharedInstance.play()
    }
    
    func stopPlayList() {
        AudioManager.sharedInstance.pause()
    }
}

extension PlayerViewController: AudioManagerDelegate {
    func audioManagerDidStartToPlayAudio(audio audioViewModel: AudioViewModel) {
        self.currentCoverImage.image = audioViewModel.coverImage
        self.audioNameLabel.text = audioViewModel.titleString
        self.artistNameLabel.text = audioViewModel.authorString
        self.albumNameLabel.text = audioViewModel.albumString
        self.currentTimeLabel.text = "00:00"
        self.durationLabel.text = audioViewModel.durationString
    }
    
    func audioManagerDidUpdateTime(timeString: String, progress: Float) {
        self.currentTimeLabel.text = timeString
        self.proggressView.progress = progress
    }
    

    func audioManagerPlayerStatusChanged(active: Bool) {
        //updating UI
        DispatchQueue.main.async {
            self.playButton.isSelected = active
        }
    }
}
