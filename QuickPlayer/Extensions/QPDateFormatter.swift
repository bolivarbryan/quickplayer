//
//  QPDateFormatter.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import Foundation

struct QPTimeFormatter {
    
    static func intToTimeString(timeInSeconds: Int) -> String {
        let minutes =  timeInSeconds / 60
        let seconds = (timeInSeconds % 3600) % 60
        let minutesString = minutes > 9 ? "\(minutes)" : "0\(minutes)"
        let secondsString = seconds > 9 ? "\(seconds)" : "0\(seconds)"
        return "\(minutesString):\(secondsString)"
    }
    
}
