//
//  QPString.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import Foundation

extension String {
    
    var capitalizeFirstCharacter:String {
        let firstCharacter = String(characters.prefix(1))
        return firstCharacter.uppercased() + String(characters.dropFirst())
    }
 


}
