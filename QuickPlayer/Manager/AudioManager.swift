//
//  AudioManager.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import Foundation
import AudioToolbox
import AVFoundation
import MediaPlayer

protocol AudioManagerDelegate {
    func audioManagerDidStartToPlayAudio(audio: AudioViewModel)
    func audioManagerDidUpdateTime(timeString: String, progress:Float)
    func audioManagerPlayerStatusChanged(active: Bool)
}

class AudioManager:NSObject {
    //MARK: Properties
    private var notPlayedAudioUrls: [String] = []
    var delegate:AudioManagerDelegate! = nil
    var playList: [Audio] = []
    private var audioPlayer:AVAudioPlayer?
    var audioCount: Int {
        return self.notPlayedAudioUrls.count
    }
    
    static let sharedInstance: AudioManager = AudioManager()
    
    private var audioUrls: [String] {
        get {
            let url = Bundle.main.resourceURL!
            do {
                // Get the directory contents urls (including subfolders urls)
                let directoryContents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys:[], options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles)
                let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" }
                let mp3FileNames = mp3Files.flatMap({$0.deletingPathExtension().lastPathComponent })
                
                return mp3FileNames
            } catch {
                print(error)
                return []
            }
        }
    }
    

    private override init() {
        super.init()
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(moveTimer), userInfo: nil, repeats: true)
        UIApplication.shared.beginReceivingRemoteControlEvents()
    }
    
   
    
    
    var currentTime:String {
        let currentTime = Int(ceil((audioPlayer?.currentTime)!))
        return QPTimeFormatter.intToTimeString(timeInSeconds: currentTime)
    }
    
    //MARK: Functions
    
     func play() {
        if audioPlayer == nil {
            self.resetPlayer()
            //in case there is not any audio file in project
            guard notPlayedAudioUrls.count > 0 else {
                return
            }
            
            self.reproduceAudioFile()
        } else {
            self.resume()
        }
    }
    
    //observe the elapsed time and send update notification
    func moveTimer() {
        guard let player = self.audioPlayer else {
            return
        }
        
        let proggress = (player.currentTime)/(player.duration)
        delegate.audioManagerDidUpdateTime(timeString: currentTime, progress: Float(proggress))
    }
 
    // MARK: Player Functions
    func pause() {
        self.audioPlayer?.pause()
        delegate.audioManagerPlayerStatusChanged(active: false)
    }
    
    //resume playback
    func resume() {
        DispatchQueue.main.async {
            self.audioPlayer?.play()
            self.delegate.audioManagerPlayerStatusChanged(active: true)
        }
    }
    
    //toggle audio
    func toggle() {
        if self.audioPlayer?.isPlaying == true {
            self.pause()
        } else {
            self.resume()
        }
    }
    
    //next audio
    func next() {
        self.reproduceAudioFile()
        self.delegate.audioManagerPlayerStatusChanged(active: true)
    }
    
    
    func resetPlayer() {
        //used for reset the playlist when all audio files were played.
        notPlayedAudioUrls = audioUrls
    }
    
    func getAudioMetaData(fromUrl url: URL) -> AudioViewModel {
        let playerItem = AVPlayerItem(url: url)
        let metadataList = playerItem.asset.commonMetadata
        var audio = Audio(title: "", durationInSeconds: 0)
        let audioInfo = MPNowPlayingInfoCenter.default()
        var nowPlayingInfo:[String:AnyObject] = [:]

        for item in metadataList {
            if let value = item.value as? String {
                
                switch item.commonKey! {
                case "title":
                    audio.title = value
                    
                case "albumName":
                    audio.album = value
                case "artist":
                    audio.author = value
                default:
                    break
                }
            } else if let value = item.value as? Data {
                audio.coverImage = value
            }
        }
        audio.durationInSeconds = Int(ceil((audioPlayer?.duration)!))
        let audioViewModel = AudioViewModel(audio: audio)

        
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = self.audioPlayer?.currentTime as AnyObject
        nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: audioViewModel.coverImage)
        nowPlayingInfo[MPMediaItemPropertyArtist] = audioViewModel.authorString as AnyObject
        nowPlayingInfo[MPMediaItemPropertyAlbumTitle] = audioViewModel.albumString as AnyObject
        nowPlayingInfo[MPMediaItemPropertyTitle] = audioViewModel.titleString as AnyObject
        audioInfo.nowPlayingInfo = nowPlayingInfo

        return audioViewModel
    }
    
    func reproduceAudioFile() {
        if self.notPlayedAudioUrls.count == 0 {
            self.resetPlayer()
        }
        
        let randomIndex = Int(arc4random_uniform(UInt32(notPlayedAudioUrls.count)))
        let url = Bundle.main.url(forResource: notPlayedAudioUrls[randomIndex], withExtension: "mp3")!
        notPlayedAudioUrls.remove(at: randomIndex)
        
        
        do {
            //FIXME: Implement a better solution here :-)
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch  {
            print("error")
        }
        
        DispatchQueue.main.async {
            self.audioPlayer?.prepareToPlay()
            self.audioPlayer?.play()
        }
        delegate.audioManagerDidStartToPlayAudio(audio: self.getAudioMetaData(fromUrl: url))
    }
    
    
}
