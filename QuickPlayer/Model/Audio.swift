//
//  Audio.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import Foundation

struct Audio {
    var title: String
    var author: String?
    var coverImage: Data?
    var durationInSeconds: Int
    var album: String?
    
    init(title: String, author: String? = nil, coverImage: Data? = nil, album: String? = nil, durationInSeconds: Int) {
        self.title = title
        self.author = author
        self.coverImage = coverImage
        self.durationInSeconds = durationInSeconds
        self.album = album
    }
}
