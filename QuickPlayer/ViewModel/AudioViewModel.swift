//
//  AudioViewModel.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import Foundation
import UIKit

struct AudioViewModel {
    private var audio: Audio
    
    var titleString: String {
        return self.audio.title.capitalizeFirstCharacter
    }
    
    var durationString: String {
        return QPTimeFormatter.intToTimeString(timeInSeconds: self.audio.durationInSeconds)
    }
    
    var authorString: String {
        guard let author = self.audio.author else {
            return "-"
        }
        
        return author.capitalized
    }
    
    var coverImage: UIImage {
        guard let data = audio.coverImage else {
            return UIImage(named: "default_cover_image")!
        }
        
        return UIImage(data: data)!
    }
    
    var albumString: String {
        
        guard let name = self.audio.album else {
            return "-"
        }
        
        return name.uppercased()
    }
    
    
    init(audio: Audio) {
        self.audio = audio
    }
    
}
