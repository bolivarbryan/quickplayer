//
//  AudioManager.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import XCTest
@testable import QuickPlayer

class AudioManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testAudioCount_Intially_ShouldBeZero()  {
        let audioManager = AudioManager.sharedInstance
        XCTAssertEqual(audioManager.audioCount, 0, "Initially Audio count should be 0")
    }


    func testAudioFiles_ShouldBeLocalized() {
    }
}
