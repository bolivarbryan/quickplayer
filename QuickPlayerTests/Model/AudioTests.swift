//
//  AudioTests.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import XCTest
@testable import QuickPlayer

class AudioTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInit_ShouldSetTitle() {
        let expectedTitle = "Don't stop me now"
        let audio = Audio(title: "Don't stop me now", durationInSeconds: 100)
        
        XCTAssertEqual(audio.title, expectedTitle)
    }
    
    func testInit_ShouldSetAuthor() {
        let expectedAuthor = "Queen"
        let audio = Audio(title: "Don't stop me now", author: "Queen", durationInSeconds: 100)
        
        XCTAssertEqual(audio.author, expectedAuthor)
    }
    
    func testInit_ShouldSetCoverImage() {
        //using image called queen, took from assets
        let expectedImage = UIImagePNGRepresentation(UIImage(named:"queen")!)
        let audio = Audio(title: "Don't stop me now", author: "Queen", coverImage: UIImagePNGRepresentation(UIImage(named:"queen")!), durationInSeconds: 100)
        
        XCTAssertEqual(audio.coverImage, expectedImage)
    }
    
    func testInit_ShouldSetDuration() {
        let expectedDurationInSeconds = 100
        let audio = Audio(title: "Don't stop me now", durationInSeconds: 100)
        
        XCTAssertEqual(audio.durationInSeconds, expectedDurationInSeconds)
    }
    
    func testInit_ShoutSetAlbum() {
        let expectedString = "random access memories"
        let audio = Audio(title: "get lucky", album: "random access memories", durationInSeconds: 360)
        
        XCTAssertEqual(audio.album, expectedString)
    }

    
}
