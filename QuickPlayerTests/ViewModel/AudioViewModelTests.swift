//
//  AudioViewModelTests.swift
//  QuickPlayer
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import XCTest
@testable import QuickPlayer

class AudioViewModelTests: XCTestCase {
    var audio:Audio!
    
    override func setUp() {
        super.setUp()
        self.audio = Audio(title: "don't stop me now", author: "Queen", coverImage: UIImagePNGRepresentation(UIImage(named:"queen")!), durationInSeconds: 100)
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testTitle_ShouldReturnTitleFirstCharacerCapitalized () {
        let expectedTitle = "Don't stop me now"
        let audioViewModel = AudioViewModel(audio: audio)
        XCTAssertEqual(audioViewModel.titleString, expectedTitle)
    }
    
    func testDurationString_ShouldReturnInMinutesAndSeconds() {
        let expectedDurationString = "01:40"
        let audioViewModel = AudioViewModel(audio: audio)
        
        XCTAssertEqual(audioViewModel.durationString, expectedDurationString)
    }
    
    func testDurationString_twoHours() {
        let expectedDurationString = "120:00"
        let twoHoursAudio = Audio(title: "don't stop me now", author: "Queen", coverImage: UIImagePNGRepresentation(UIImage(named:"queen")!), durationInSeconds: 7200)
        let audioViewModel = AudioViewModel(audio: twoHoursAudio)
        
        XCTAssertEqual(audioViewModel.durationString, expectedDurationString)
    }
    
    func testAuthorString_ShouldReturnCapitalized() {
        let expectedAuthorName = "Dash Berlin"
        let audio = Audio(title: "without the sun", author: "dash berlin", coverImage: nil, durationInSeconds: 360)
        let audioViewModel = AudioViewModel(audio: audio)
        
        XCTAssertEqual(audioViewModel.authorString, expectedAuthorName)
    }
    
    func testEmptyCoverImage_shouldReturnADefaultImage() {
        let expectedDetaultImage = UIImage(named:"default_cover_image")!
        let audio = Audio(title: "without the sun", author: "dash berlin", coverImage: nil, durationInSeconds: 360)
        let audioViewModel = AudioViewModel(audio: audio)
        
        XCTAssertEqual(audioViewModel.coverImage, expectedDetaultImage)
    }

    func testAlbumString_ShouldReturnUppercased()  {
        let expectedAlbumName = "RANDOM ACCESS MEMORIES"
        let audio = Audio(title: "get lucky", author: "Daft punk", coverImage: nil, album: "random access memories" , durationInSeconds: 360)
        let audioViewModel = AudioViewModel(audio: audio)

        XCTAssertEqual(audioViewModel.albumString, expectedAlbumName)
    }
    
    func testEmptyAuthor_ShouldReturnLineCharacter() {
        let expectedAuthorName = "-"
        let audio = Audio(title: "get lucky", coverImage: nil, album: "random access memories" , durationInSeconds: 360)
        let audioViewModel = AudioViewModel(audio: audio)
        XCTAssertEqual(audioViewModel.authorString, expectedAuthorName)
    }
}
