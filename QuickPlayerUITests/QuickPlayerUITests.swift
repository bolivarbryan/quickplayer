//
//  QuickPlayerUITests.swift
//  QuickPlayerUITests
//
//  Created by Bryan on 4/6/17.
//  Copyright © 2017 bolivarbryan. All rights reserved.
//

import XCTest

class QuickPlayerUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAlbumText_ShouldChangeAfterNextButtonIsTapped () {
        // Use recording to get started writing UI tests.
        
        let app = XCUIApplication()
        let playerbuttonNextButton = app.buttons["PlayerButton Next"]
        playerbuttonNextButton.tap()
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertFalse(app.staticTexts["Bryan Bolivar"].exists)
    }
    
    func testAlbumText_ShouldChangeAfterBackButtonIsTapped () {
        // Use recording to get started writing UI tests.
        
        let app = XCUIApplication()
        let playerbuttonNextButton = app.buttons["PlayerButton Before"]
        playerbuttonNextButton.tap()
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertFalse(app.staticTexts["Bryan Bolivar"].exists)
    }
    
    func testAlbumText_ShouldChangeAfterPlayButtonIsTapped () {
        // Use recording to get started writing UI tests.
        
        let app = XCUIApplication()
        let playerbuttonNextButton = app.buttons["PlayerButton Play"]
        playerbuttonNextButton.tap()
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertFalse(app.staticTexts["Bryan Bolivar"].exists)
    }
    
}
