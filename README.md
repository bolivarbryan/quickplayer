# QuickPlayer #

### Patterns used ###

* MVVM
* Singleton
* Decorator


### Unit Tests included for ###

* Audio
* AudioManager
* AudioViewModel

### UI Tests included for ###
* Player buttons
* Audio Album name

### UI/UX ###
* Sketch, file included too

### About Author ###

* Bryan Bolivar, Software Engineer (iOS) since 2010, https://www.linkedin.com/in/bolivarbryan